<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/uses', function (Request $request) {
    return $request->uses();
});

Route::prefix('v1')->group(function () {
    Route::get('health_check', [
        'uses' => 'API\HealthCheckController@get',
        'as' => 'api/v1/health_check'
    ]);

    Route::prefix('users')->group(function () {
        Route::get('get/{id}', [
            'uses' => 'API\UserController@get',
            'as' => 'api/v1/users/get'
        ]);

        Route::get('projects/{id}/{project_id?}', [
            'uses' => 'API\UserController@getProjects',
            'as' => 'api/v1/users/projects/'
        ]);

        Route::get('department/get/{id}', [
            'uses' => 'API\UserController@getDepartment',
            'as' => 'api/v1/users/department/get'
        ]);

        Route::get('tasks/{id}', [
            'uses' => 'API\UserController@getTask',
            'as' => 'api/v1/users/tasks'
        ]);

        Route::get('logs/get/{id}', [
            'uses' => 'API\UserController@getLogs',
            'as' => 'api/v1/users/logs/get'
        ]);

    });

    Route::prefix('formulas')->group(function () {
        Route::get('{id}', [
            'uses' => 'API\FormulaController@get',
            'as' => 'api/v1/formulas/get'
        ]);

        Route::get('', [
            'uses' => 'API\FormulaController@all',
            'as' => 'api/v1/formulas/all'
        ]);

        Route::post('', [
            'uses' => 'API\FormulaController@post',
            'as' => 'api/v1/formulas/post/'
        ]);

        Route::patch('', [
            'uses' => 'API\FormulaController@patch',
            'as' => 'api/v1/formulas/patch'
        ]);

        Route::delete('', [
            'uses' => 'API\FormulaController@delete',
            'as' => 'api/v1/formulas/delete'
        ]);


    });
});

