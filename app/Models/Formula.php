<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    protected $fillable = ['name', 'description', 'default'];

    protected $attributes = [
        'default' => false,
    ];
}
