<?php


namespace App\Decorators;


use App\Services\Service;

interface Decorator extends Service
{
    public function createService(): Service;
}