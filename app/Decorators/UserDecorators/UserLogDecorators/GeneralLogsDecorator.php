<?php


namespace App\Decorators\UserDecorators\UserLogDecorators;


use App\Decorators\UserDecorators\EloquentUserDecorator;
use Illuminate\Database\Eloquent\Model;

class GeneralLogsDecorator extends EloquentUserDecorator
{
    public function getModel(int $id, array $relations = [], array $attributes = []): ?Model
    {
        $logs = [
            [
                'name' => 'Personal Department KPI 6-2019',
                'time' => '2019-10-15 14:15:31'
            ],
            [
                'name' => 'Personal Department KPI 6-2019',
                'time' => '2019-10-15 14:17:05'
            ],
            [
                'name' => 'Personal Department KPI 6-2019',
                'time' => '2019-10-15 14:17:09'
            ],
            [
                'name' => 'Personal Department KPI 7-2019',
                'time' => '2019-10-15 14:17:19'
            ],
            [
                'name' => 'Personal Department KPI 7-2019',
                'time' => '2019-10-15 14:17:19'
            ],
            [
                'name' => 'Personal Department KPI 11-2019',
                'time' => '2019-10-15 16:15:02'
            ],
            [
                'name' => 'Personal Department KPI 11-2019',
                'time' => '2019-10-15 16:15:10'
            ],
            [
                'name' => 'Personal Department KPI 10-2019',
                'time' => '2019-10-15 16:17:21'
            ],

        ];
        $user = parent::getModel($id, $relations, $attributes);
        if ($user!=null) {
            $user['logs'] = $logs;
        }
        return $user;
    }
}
