<?php


namespace App\Decorators\UserDecorators;


use App\Decorators\EloquentDecorator;
use App\Services\EloquentServices\EloquentUserService;
use App\Services\Service;

class EloquentUserDecorator extends EloquentDecorator implements UserDecorator
{

    public function createService(): Service
    {
        return new EloquentUserService();
    }
}
