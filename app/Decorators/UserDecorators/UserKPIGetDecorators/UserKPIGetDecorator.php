<?php


namespace App\Decorators\UserDecorators\UserKPIGetDecorators;


use App\Decorators\UserDecorators\EloquentUserDecorator;
use App\Handlers\EloquentHandlers\EloquentUserHandlers\UserProjectHandlers\MultipleUserProjectHandler;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserKPIGetDecorator extends EloquentUserDecorator
{
    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model
    {
        $user = new User();

        $from = date('Y-m-d', strtotime($attributes['from']));
        $to = date('Y-m-d', strtotime($attributes['to']));
        $user['from'] = $from;
        $user['to'] = $to;
        $user['id'] =$id;

        $recurrentTasks = json_decode(
            file_get_contents(
                'https://falling-frog-38743.pktriot.net/api/recurrent-tasks/users/'
                . $id . '?start=' . $from . 'T00:00:00.000z&finish=' . $to . 'T00:00:00.000z'
            )
            , true);
        foreach ($recurrentTasks as &$recurrentTask) {
            $recurrentTask = $this->formatRecurrentTask($recurrentTask);
        }

        $handler = new MultipleUserProjectHandler();
        $attributes['user_id'] = $id;
        $response = $handler->handle($attributes);

        $user['KPI'] = ['projects' =>$response['data']['projects'], 'tasks' => $recurrentTasks];
        unset($response['data']['from']);
        unset($response['data']['to']);
        return $user;
    }

    protected function formatRecurrentTask($task): array
    {
        $tmp['id'] = $task['_id'];
        $tmp['name'] = $task['name'];
        $tmp['description'] = $task['description'];
        $tmp['complete_percent'] = $task['percentComplete'];
        $tmp['KPI'] = $task['percentComplete'] / 100;

        return $tmp;
    }
}
