<?php


namespace App\Decorators\UserDecorators\UserKPIGetDecorators;


use App\Decorators\UserDecorators\EloquentUserDecorator;
use App\Handlers\EloquentHandlers\EloquentUserHandlers\UserDepartmentHandler;
use App\Handlers\EloquentHandlers\EloquentUserHandlers\UserKPICalculateHandler;
use Illuminate\Database\Eloquent\Model;

class UserKPIDepartmentGetDecorator extends EloquentUserDecorator
{
    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model
    {
        $user = parent::getModel($id, [], $attributes);
        if ($user!= null) {
            $userKPIHandler = new UserKPICalculateHandler();
            $departmentKPIHandler = new UserDepartmentHandler();

            $userKPIHandler->setNextHandler($departmentKPIHandler);

            $attributes['relations'] = $relations;
            $response = $userKPIHandler->handle($attributes);

            $user['KPI'] = $response['data'];
        }

        return $user;
    }
}
