<?php


namespace App\Decorators\UserDecorators\UserKPIGetDecorators\UserProjectDecorators;


use App\Decorators\UserDecorators\EloquentUserDecorator;
use App\Handlers\EloquentHandlers\EloquentUserHandlers\UserProjectHandlers\SingleUserProjectHandler;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserProjectKPIDecorator extends EloquentUserDecorator
{
    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model
    {
        $user = new User();
        $projectKPIHandler = new SingleUserProjectHandler();
        $attributes['user_id'] = $id;
        $response = $projectKPIHandler->handle($attributes);

        $responseData = $response['data'];
        unset($responseData['relations']);

        foreach ($responseData as $key => $value) {
            $user[$key] = $value;
        }


        return $user;
    }
}
