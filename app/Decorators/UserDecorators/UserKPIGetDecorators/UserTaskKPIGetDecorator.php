<?php


namespace App\Decorators\UserDecorators\UserKPIGetDecorators;


use App\Decorators\UserDecorators\EloquentUserDecorator;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UserTaskKPIGetDecorator extends EloquentUserDecorator
{

    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model
    {
        $user = new User();
        $user['id'] = $id;
        if (isset($attributes['task_id'])) {
            $recurrentTask = json_decode(
                file_get_contents(
                    'https://falling-frog-38743.pktriot.net/api/recurrent-tasks/'. $attributes['task_id']
                ), true);
            if (strcasecmp($recurrentTask['doer']['id'], $id) == 0) {
                $user['task'] = $this->formatRecurrentTask($recurrentTask);
            } else {
                $user['task'] = [];
            }
        } else {
            $from = date('Y-m-d', strtotime($attributes['from']));
            $to = date('Y-m-d', strtotime($attributes['to']));
            $user['from'] = $from;
            $user['to'] = $to;
            $recurrentTasks = json_decode(
                file_get_contents(
                    'https://falling-frog-38743.pktriot.net/api/recurrent-tasks/users/'
                    . $id . '?start=' . $from . 'T00:00:00.000z&finish=' . $to . 'T00:00:00.000z'
                )
                , true);

            foreach ($recurrentTasks as &$recurrentTask) {
                $recurrentTask = $this->formatRecurrentTask($recurrentTask);
            }
            $user['tasks'] = $recurrentTasks;
        }
        return $user;
    }

    protected function formatRecurrentTask($task): array
    {
        $tmp['id'] = $task['_id'];
        $tmp['name'] = $task['name'];
        $tmp['description'] = $task['description'];
        $tmp['complete_percent'] = $task['percentComplete'];
        $tmp['KPI'] = $task['percentComplete'] / 100;

        return $tmp;
    }
}
