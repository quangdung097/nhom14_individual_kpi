<?php


namespace App\Decorators;


use App\Services\Message;
use App\Services\Service;
use Illuminate\Database\Eloquent\Model;

abstract class EloquentDecorator implements Decorator
{
    protected $service;

    private $message;

    public function __construct()
    {
        $this->service = $this->createService();
    }

    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model
    {
        return $this->service->getModel($id, $relations);
    }

    public function createNewModel(array $attributes): ?Model
    {
        return $this->service->createNewModel($attributes);
    }

    public function updateModel(int $id, array $attributes): bool
    {
        return $this->service->updateModel($id, $attributes);
    }

    public function deleteModel(int $id): bool
    {
        return $this->service->deleteModel($id);
    }

    public function count(array $attributes = []): int
    {
        return $this->service->count($attributes);
    }

    public function getAll(string $sort = 'id',
                           string $order = 'asc', int $limit = 5,
                           int $offset = 0, array $relations = [])
    {
        return $this->service->getAll($sort, $order, $limit, $offset, $relations);
    }

    public function findModels(array $pairs = [], array $relations = [])
    {
        return $this->service->findModels($pairs, $relations);
    }

    public function getMessage(): string
    {
        $serviceMessage = $this->toMessage($this->service);
        if ($serviceMessage) {
            return $serviceMessage;
        }
        return $this->message;
    }

    public function setService(string $message)
    {
        $this->message = $message;
    }

    protected function toMessage(Service $service): string
    {
        /**
         * @var Message $service
         */
        return $service->getMessage();
    }


}
