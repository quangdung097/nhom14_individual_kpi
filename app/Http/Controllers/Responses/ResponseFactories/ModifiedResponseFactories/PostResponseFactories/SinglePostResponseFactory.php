<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PostResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\PostResponses\PostSingleErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\PostResponses\PostSingleSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\BaseSingleModifiedResponseFactory;

class SinglePostResponseFactory extends BaseSingleModifiedResponseFactory
{

    protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse
    {
        return new PostSingleSuccessResponse($responseData);
    }

    protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse
    {
        return new PostSingleErrorResponse($responseData);
    }
}