<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PostResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\PostResponses\PostListErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\PostResponses\PostListPartialResponse;
use App\Http\Controllers\Responses\ImpResponses\PostResponses\PostListSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\BaseListModifiedResponseFactory;

class ListPostResponseFactory extends BaseListModifiedResponseFactory
{

    protected function createPartialResponse(int $success = 0, int $error = 0, $responseData = null): BaseDataResponse
    {
        return new PostListPartialResponse($success, $error, $responseData);
    }

    protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse
    {
        return new PostListSuccessResponse($success, $responseData);
    }

    protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse
    {
        return new PostListErrorResponse($failed, $responseData);
    }
}