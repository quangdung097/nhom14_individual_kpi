<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;

abstract class BaseListModifiedResponseFactory extends BaseModifiedResponseFactory
{
    abstract protected function createPartialResponse(int $success = 0, int $error = 0, $responseData = null): BaseDataResponse;

    public function getResponse(int $success = 0, int $failed = 0, array $attributes = []): BaseDataResponse
    {
        $responseData = $this->getResponseData($attributes);
        if ($success == 0) {
            return $this->createErrorResponse($failed, $responseData);
        } else if ($failed == 0) {
            return $this->createSuccessResponse($success, $responseData);
        } else {
            return $this->createPartialResponse($success, $failed, $responseData);
        }
    }

    private function getResponseData(array $attributes): array
    {
        if (array_key_exists('list_data', $attributes)) {
            $list_data = $attributes['list_data'];
            if (is_array($list_data)) {
                return $list_data;
            }
            return [$list_data];
        }
        return [];
    }
}