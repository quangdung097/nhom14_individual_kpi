<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ResponseFactories\ResponseFactory;

abstract class BaseModifiedResponseFactory implements ResponseFactory
{
    abstract protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse;

    abstract protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse;
}