<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\DeleteResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\DeleteResponse\DeleteSingleErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\DeleteResponse\DeleteSingleSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\BaseSingleModifiedResponseFactory;

class SingleDeleteResponseFactory extends BaseSingleModifiedResponseFactory
{

    protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse
    {
        return new DeleteSingleSuccessResponse($responseData);
    }

    protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse
    {
        return new DeleteSingleErrorResponse($responseData);
    }
}