<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\DeleteResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\DeleteResponse\DeleteListErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\DeleteResponse\DeleteListPartialResponse;
use App\Http\Controllers\Responses\ImpResponses\DeleteResponse\DeleteListSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\BaseListModifiedResponseFactory;

class ListDeleteResponseFactory extends BaseListModifiedResponseFactory
{

    protected function createPartialResponse(int $success = 0, int $error = 0, $responseData = null): BaseDataResponse
    {
        return new DeleteListPartialResponse($success, $error, $responseData);
    }

    protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse
    {
        return new DeleteListSuccessResponse($success, $responseData);
    }

    protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse
    {
        return new DeleteListErrorResponse($failed, $responseData);
    }
}