<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PatchResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\PatchResponse\PatchSingleErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\PatchResponse\PatchSingleSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\BaseSingleModifiedResponseFactory;

class SinglePatchResponseFactory extends BaseSingleModifiedResponseFactory
{

    protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse
    {
        return new PatchSingleSuccessResponse($responseData);
    }

    protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse
    {
        return new PatchSingleErrorResponse($responseData);
    }
}