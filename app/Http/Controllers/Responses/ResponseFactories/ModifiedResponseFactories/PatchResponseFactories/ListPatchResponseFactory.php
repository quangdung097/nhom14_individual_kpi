<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PatchResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\PatchResponse\PatchListErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\PatchResponse\PatchListPartialResponse;
use App\Http\Controllers\Responses\ImpResponses\PatchResponse\PatchListSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\BaseListModifiedResponseFactory;

class ListPatchResponseFactory extends BaseListModifiedResponseFactory
{

    protected function createPartialResponse(int $success = 0, int $error = 0, $responseData = null): BaseDataResponse
    {
        return new PatchListPartialResponse($success, $error, $responseData);
    }

    protected function createSuccessResponse(int $success = 0, $responseData = null): BaseDataResponse
    {
        return new PatchListSuccessResponse($success, $responseData);
    }

    protected function createErrorResponse(int $failed = 0, $responseData = null): BaseDataResponse
    {
        return new PatchListErrorResponse($failed, $responseData);
    }
}