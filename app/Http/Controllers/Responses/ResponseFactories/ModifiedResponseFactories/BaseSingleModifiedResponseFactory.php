<?php


namespace App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;

abstract class BaseSingleModifiedResponseFactory extends BaseModifiedResponseFactory
{
    public function getResponse(int $success = 0, int $failed = 0, array $attributes = []): BaseDataResponse
    {
        $responseData = $attributes['data'] == null ? $attributes['data'] : null;
        if ($success > 0) {
            return $this->createSuccessResponse(1, $responseData);
        } else {
            return $this->createErrorResponse(1, $responseData);
        }
    }
}