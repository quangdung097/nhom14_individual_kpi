<?php


namespace App\Http\Controllers\Responses\ResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;

interface ResponseFactory
{
    public function getResponse(int $success = 0, int $failed = 0, array $attributes = []): BaseDataResponse;
}