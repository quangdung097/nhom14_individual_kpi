<?php


namespace App\Http\Controllers\Responses\ResponseFactories\GetResponseFactories;


use App\Http\Controllers\Responses\BaseDataResponse;
use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetListErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetListSuccessResponse;
use App\Http\Controllers\Responses\ResponseFactories\ResponseFactory;

abstract class BaseGetResponseFactory implements ResponseFactory
{
    abstract protected function createSuccessGetResponse($success = 0, $responseData = null): GetListSuccessResponse;

    abstract protected function createErrorGetResponse($failed = 0, $responseData = null): GetListErrorResponse;

    public function getResponse(int $success = 0, int $failed = 0, array $attributes = []): BaseDataResponse
    {
        if (array_key_exists('data', $attributes)) {
            $responseData = $attributes['data'];
        } else {
            $responseData = null;
        }
        if ($success > 0) {
            return $this->createSuccessGetResponse($success, $responseData);
        }
        return $this->createErrorGetResponse($failed, $responseData);
    }
}