<?php


namespace App\Http\Controllers\Responses\ResponseFactories\GetResponseFactories;


use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetListErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetListSuccessResponse;
use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetSingleErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetSingleSuccessResponse;

class SingleGetResponseFactory extends BaseGetResponseFactory
{

    protected function createSuccessGetResponse($success = 0, $responseData = null): GetListSuccessResponse
    {
        return new GetSingleSuccessResponse($responseData);
    }

    protected function createErrorGetResponse($failed = 0, $responseData = null): GetListErrorResponse
    {
        return new GetSingleErrorResponse($responseData);
    }
}