<?php


namespace App\Http\Controllers\Responses\ResponseFactories\GetResponseFactories;


use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetListErrorResponse;
use App\Http\Controllers\Responses\ImpResponses\GetResponses\GetListSuccessResponse;

class ListGetResponseFactory extends BaseGetResponseFactory
{

    protected function createSuccessGetResponse($success = 0, $responseData = null): GetListSuccessResponse
    {
        return new GetListSuccessResponse($success, $responseData);
    }

    protected function createErrorGetResponse($failed = 0, $responseData = null): GetListErrorResponse
    {
        return new GetListErrorResponse($failed, $responseData);
    }

}