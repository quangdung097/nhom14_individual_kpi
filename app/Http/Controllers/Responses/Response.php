<?php


namespace App\Http\Controllers\Responses;


interface Response
{
    public function serialize(array $attributes = null);

    public function response();
}