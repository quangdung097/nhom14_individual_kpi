<?php


namespace App\Http\Controllers\Responses;


class BaseResponse implements Response
{
    private $responseCode;
    private $responseMessage;

    public function __construct(int $responseCode, string $responseMessage)
    {
        $this->responseCode = $responseCode;
        $this->responseMessage = $responseMessage;
    }

    public function serialize(array $attributes = null): array
    {
        return [
            'code' => $this->responseCode,
            'message' => $this->responseMessage
        ];
    }

    public function response(array $attributes = null)
    {
        return response()->json($this->serialize($attributes), $this->responseCode);
    }

    public function setResponseCode(int $responseCode)
    {
        $this->responseCode = $responseCode;
    }

    public function setResponseMessage(string $responseMessage)
    {
        $this->responseMessage = $responseMessage;
    }

    public function getResponseCode(): int
    {
        return $this->responseCode;
    }

    public function getResponseMessage(): string
    {
        return $this->responseMessage;
    }
}