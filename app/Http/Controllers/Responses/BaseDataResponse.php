<?php


namespace App\Http\Controllers\Responses;


class BaseDataResponse extends BaseResponse
{
    private $responseData;

    public function __construct(int $responseCode, string $responseMessage, array $responseData)
    {
        parent::__construct($responseCode, $responseMessage);
        $this->responseData = $responseData;
    }

    public function serialize(array $attributes = null): array
    {
        return array_merge(
            parent::serialize($attributes),
            ['data' => $this->responseData]
        );
    }

    public function setResponseData($responseData)
    {
        $this->responseData = $responseData;
    }

    public function getResponseData(): array
    {
        return $this->responseData;
    }
}