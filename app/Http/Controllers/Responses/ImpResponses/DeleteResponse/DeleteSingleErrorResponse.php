<?php


namespace App\Http\Controllers\Responses\ImpResponses\DeleteResponse;


class DeleteSingleErrorResponse extends DeleteListErrorResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Deleted failed')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}