<?php


namespace App\Http\Controllers\Responses\ImpResponses\DeleteResponse;


class DeleteListErrorResponse extends DeleteListPartialResponse
{
    public function __construct(int $errors, array $responseData = [],
                                string $responseMessage = 'List deleted failed')
    {
        parent::__construct(0, $errors, $responseData, $responseMessage);
        $this->setResponseCode(400);
    }
}