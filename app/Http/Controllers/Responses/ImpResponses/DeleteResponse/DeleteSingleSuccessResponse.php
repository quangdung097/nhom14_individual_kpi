<?php


namespace App\Http\Controllers\Responses\ImpResponses\DeleteResponse;


class DeleteSingleSuccessResponse extends DeleteListSuccessResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Deleted successfully')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}