<?php


namespace App\Http\Controllers\Responses\ImpResponses\DeleteResponse;


use App\Http\Controllers\Responses\ImpResponses\BaseListResponse;

class DeleteListPartialResponse extends BaseListResponse
{
    public function __construct(int $success, int $errors, array $responseData = [],
                                string $responseMessage = 'List deleted partially')
    {
        parent::__construct($success, $errors, $responseData, $responseMessage);
        $this->setResponseCode(207);
    }
}