<?php


namespace App\Http\Controllers\Responses\ImpResponses\DeleteResponse;


class DeleteListSuccessResponse extends DeleteListPartialResponse
{
    public function __construct(int $success, array $responseData = [],
                                string $responseMessage = 'List deleted successfully')
    {
        parent::__construct($success, 0, $responseData, $responseMessage);
        $this->setResponseCode(200);
    }
}