<?php


namespace App\Http\Controllers\Responses\ImpResponses\GetResponses;


class GetListErrorResponse extends GetListPartialResponse
{
    public function __construct(int $errors, array $responseData = [], string $responseMessage = 'Not Found')
    {
        parent::__construct(0, $errors, $responseData, $responseMessage);
        $this->setResponseCode(404);
    }
}