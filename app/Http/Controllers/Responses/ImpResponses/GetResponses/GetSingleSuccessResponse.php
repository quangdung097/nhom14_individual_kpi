<?php


namespace App\Http\Controllers\Responses\ImpResponses\GetResponses;


class GetSingleSuccessResponse extends GetListSuccessResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Found')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}