<?php


namespace App\Http\Controllers\Responses\ImpResponses\GetResponses;


class GetListSuccessResponse extends GetListPartialResponse
{
    public function __construct(int $success, array $responseData = [], string $responseMessage = 'List found')
    {
        parent::__construct($success, 0, $responseData, $responseMessage);
        $this->setResponseCode(200);
    }
}