<?php


namespace App\Http\Controllers\Responses\ImpResponses\GetResponses;


class GetSingleErrorResponse extends GetListErrorResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Not Found')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}