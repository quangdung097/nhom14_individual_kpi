<?php


namespace App\Http\Controllers\Responses\ImpResponses\GetResponses;


use App\Http\Controllers\Responses\ImpResponses\BaseListResponse;

class GetListPartialResponse extends BaseListResponse
{
    public function __construct(int $success, int $errors, array $responseData = [], string $responseMessage = 'Found')
    {
        parent::__construct($success, $errors, $responseData, $responseMessage);
        $this->setResponseCode(207);
    }
}