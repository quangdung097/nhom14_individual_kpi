<?php


namespace App\Http\Controllers\Responses\ImpResponses;


use App\Http\Controllers\Responses\BaseDataResponse;

class BaseDataSuccessResponse extends BaseDataResponse
{
    public function __construct(array $responseData = [], string $responseMessage = 'Ok')
    {
        parent::__construct(200, $responseMessage, $responseData);
    }
}