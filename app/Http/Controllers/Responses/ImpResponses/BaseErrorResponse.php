<?php


namespace App\Http\Controllers\Responses\ImpResponses;

use App\Http\Controllers\Responses\BaseResponse;

class BaseErrorResponse extends BaseResponse
{
    public function __construct(string $responseMessage='Bad Request')
    {
        parent::__construct(400, $responseMessage);
    }
}