<?php


namespace App\Http\Controllers\Responses\ImpResponses;


use App\Http\Controllers\Responses\BaseResponse;

class BaseSuccessResponse extends BaseResponse
{
    public function __construct(string $responseMessage = 'Ok')
    {
        parent::__construct(200, $responseMessage);
    }
}