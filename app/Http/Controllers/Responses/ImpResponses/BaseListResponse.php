<?php


namespace App\Http\Controllers\Responses\ImpResponses;


class BaseListResponse extends BaseDataSuccessResponse
{
    private $success;
    private $errors;

    public function __construct(int $success, int $errors, array $responseData = [], string $responseMessage = 'Ok')
    {
        parent::__construct($responseData, $responseMessage);
        $this->success = $success;
        $this->errors = $errors;
    }


    public function serialize(array $attributes = null): array
    {
        return array_merge(
            parent::serialize($attributes),
            [
                'success' => $this->success,
                'error' => $this->errors
            ]
        );
    }
}