<?php


namespace App\Http\Controllers\Responses\ImpResponses;


use App\Http\Controllers\Responses\BaseDataResponse;

class BaseDataErrorResponse extends BaseDataResponse
{
    public function __construct(array $responseData = [], string $responseMessage='Bad Request')
    {
        parent::__construct(400, $responseMessage, $responseData);
    }
}