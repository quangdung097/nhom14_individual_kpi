<?php


namespace App\Http\Controllers\Responses\ImpResponses;


class BasePaginateResponse extends BaseDataSuccessResponse
{
    private $total;

    public function __construct(int $total, array $responseData = [], string $responseMessage = 'Ok')
    {
        parent::__construct($responseData, $responseMessage);
        $this->total = $total;
    }

    public function serialize(array $attributes = null): array
    {
        return array_merge(
            parent::serialize($attributes),
            ['total' => $this->total]
        );
    }
}