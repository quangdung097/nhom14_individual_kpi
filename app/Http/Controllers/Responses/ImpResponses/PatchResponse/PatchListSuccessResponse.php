<?php


namespace App\Http\Controllers\Responses\ImpResponses\PatchResponse;


class PatchListSuccessResponse extends PatchListPartialResponse
{
    public function __construct(int $success, array $responseData = [],
                                string $responseMessage = 'List updated successfully')
    {
        parent::__construct($success, 0, $responseData, $responseMessage);
        $this->setResponseCode(200);
    }
}