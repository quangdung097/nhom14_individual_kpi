<?php


namespace App\Http\Controllers\Responses\ImpResponses\PatchResponse;


use App\Http\Controllers\Responses\ImpResponses\BaseListResponse;

class PatchListPartialResponse extends BaseListResponse
{
    public function __construct(int $success, int $errors, array $responseData = [],
                                string $responseMessage = 'List updated partially')
    {
        parent::__construct($success, $errors, $responseData, $responseMessage);
        $this->setResponseCode(207);
    }
}