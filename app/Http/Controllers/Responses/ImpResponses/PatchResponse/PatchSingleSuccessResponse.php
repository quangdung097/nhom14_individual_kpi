<?php


namespace App\Http\Controllers\Responses\ImpResponses\PatchResponse;


class PatchSingleSuccessResponse extends PatchListSuccessResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Updated failed')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}