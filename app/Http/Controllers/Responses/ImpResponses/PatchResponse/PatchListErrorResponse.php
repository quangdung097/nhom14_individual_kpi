<?php


namespace App\Http\Controllers\Responses\ImpResponses\PatchResponse;


class PatchListErrorResponse extends PatchListPartialResponse
{
    public function __construct(int $errors, array $responseData = [],
                                string $responseMessage = 'List updated failed')
    {
        parent::__construct(0, $errors, $responseData, $responseMessage);
        $this->setResponseCode(400);
    }
}