<?php


namespace App\Http\Controllers\Responses\ImpResponses\PatchResponse;


class PatchSingleErrorResponse extends PatchListErrorResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Updated successfully')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}