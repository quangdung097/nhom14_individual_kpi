<?php


namespace App\Http\Controllers\Responses\ImpResponses\PostResponses;


class PostListErrorResponse extends PostListPartialResponse
{
    public function __construct(int $errors, array $responseData = [],
                                string $responseMessage = 'List created failed')
    {
        parent::__construct(0, $errors, $responseData, $responseMessage);
        $this->setResponseCode(400);
    }
}