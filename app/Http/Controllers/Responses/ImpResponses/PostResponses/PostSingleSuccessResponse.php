<?php


namespace App\Http\Controllers\Responses\ImpResponses\PostResponses;


class PostSingleSuccessResponse extends PostListSuccessResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Created successfully')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}