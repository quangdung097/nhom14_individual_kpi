<?php


namespace App\Http\Controllers\Responses\ImpResponses\PostResponses;


class PostListSuccessResponse extends PostListPartialResponse
{
    public function __construct(int $success, array $responseData = [],
                                string $responseMessage = 'List created successfully')
    {
        parent::__construct($success, 0, $responseData, $responseMessage);
        $this->setResponseCode(201);
    }
}