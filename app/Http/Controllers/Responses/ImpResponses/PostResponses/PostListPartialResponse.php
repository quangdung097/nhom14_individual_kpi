<?php


namespace App\Http\Controllers\Responses\ImpResponses\PostResponses;


use App\Http\Controllers\Responses\ImpResponses\BaseListResponse;

class PostListPartialResponse extends BaseListResponse
{
    public function __construct(int $success, int $errors, array $responseData = [],
                                string $responseMessage = 'List created partially')
    {
        parent::__construct($success, $errors, $responseData, $responseMessage);
        $this->setResponseCode(207);
    }
}