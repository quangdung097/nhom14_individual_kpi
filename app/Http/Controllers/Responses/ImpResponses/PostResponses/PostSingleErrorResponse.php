<?php


namespace App\Http\Controllers\Responses\ImpResponses\PostResponses;


class PostSingleErrorResponse extends PostListErrorResponse
{
    public function __construct($responseData = null, string $responseMessage = 'Created failed')
    {
        parent::__construct(1);
        $this->setResponseMessage($responseMessage);
        $this->setResponseData($responseData);
    }
}