<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Requests\API\FormulaRequests\FormulaDeleteRequest;
use App\Http\Controllers\Requests\API\FormulaRequests\FormulaGetRequest;
use App\Http\Controllers\Requests\API\FormulaRequests\FormulaPatchRequest;
use App\Http\Controllers\Requests\API\FormulaRequests\FormulaPostRequest;
use App\Http\Controllers\Requests\API\GetRequest;
use App\Services\EloquentServices\EloquentFormulaService;
use App\Services\Service;

class FormulaController extends BaseAPIController
{

    protected function createService(): Service
    {
        return new EloquentFormulaService();
    }

    public function get(FormulaGetRequest $request, string $id)
    {
        return parent::_get($request, $id);
    }

    public function post(FormulaPostRequest $request)
    {
        return parent::_post($request);
    }

    public function patch(FormulaPatchRequest $request)
    {
        return parent::_patch($request);
    }

    protected function all(FormulaGetRequest $request)
    {
        return parent::_all($request);
    }

    public function delete(FormulaDeleteRequest $request)
    {
        return parent::_delete($request);
    }
}
