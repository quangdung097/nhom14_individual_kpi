<?php


namespace App\Http\Controllers\API;


use App\Decorators\UserDecorators\UserKPIGetDecorators\UserKPIDepartmentGetDecorator;
use App\Decorators\UserDecorators\UserKPIGetDecorators\UserKPIGetDecorator;
use App\Decorators\UserDecorators\UserKPIGetDecorators\UserProjectDecorators\UserProjectKPIDecorator;
use App\Decorators\UserDecorators\UserKPIGetDecorators\UserProjectDecorators\UserProjectsKPIDecorator;
use App\Decorators\UserDecorators\UserKPIGetDecorators\UserTaskKPIGetDecorator;
use App\Decorators\UserDecorators\UserLogDecorators\PersonalLogDecorator;
use App\Http\Controllers\Requests\API\UserRequests\UserGetRequest;
use App\Services\EloquentServices\EloquentUserService;
use App\Services\Service;

class UserController extends BaseAPIController
{
    public function get(UserGetRequest $request, int $id = 0)
    {
        $this->service = new UserKPIGetDecorator();
        $request['from'] = $request->getFrom();
        $request['to'] = $request->getTo();
        return parent::_get($request, $id);
    }

    public function getProjects(UserGetRequest $request, string $id, string $project_id = null)
    {
        $project_id = $project_id == null ? $request->getProjectId() : $project_id;
        if ($project_id != null) {
            $request['project_id'] = $project_id;
            $this->service = new UserProjectKPIDecorator();
        } else {
            $this->service = new UserProjectsKPIDecorator();
        }

        return parent::_get($request, $id);
    }

    public function getDepartment(UserGetRequest $request, int $id = 0)
    {
        $this->service = new UserKPIDepartmentGetDecorator();
        return parent::_get($request, $id);
    }

    public function getTask(UserGetRequest $request, int $id = 0)
    {
        $this->service = new UserTaskKPIGetDecorator();
        return parent::_get($request, $id);
    }

    public function getLogs(UserGetRequest $request, int $id = 0)
    {
        $this->service = new PersonalLogDecorator();
        return parent::_get($request, $id);
    }

    protected function createService(): Service
    {
        return new EloquentUserService();
    }
}
