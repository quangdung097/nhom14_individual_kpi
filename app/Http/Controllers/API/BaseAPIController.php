<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Requests\API\DeleteRequest;
use App\Http\Controllers\Requests\API\GetRequest;
use App\Http\Controllers\Requests\API\PatchRequest;
use App\Http\Controllers\Requests\API\PostRequest;
use App\Http\Controllers\Responses\ImpResponses\BasePaginateResponse;
use App\Http\Controllers\Responses\ResponseFactories\GetResponseFactories\SingleGetResponseFactory;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\DeleteResponseFactories\ListDeleteResponseFactory;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PatchResponseFactories\ListPatchResponseFactory;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PostResponseFactories\ListPostResponseFactory;
use App\Http\Controllers\Responses\ResponseFactories\ResponseFactory;
use App\Services\Message;
use App\Services\Service;

abstract class BaseAPIController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = $this->createService();
    }

    abstract protected function createService(): Service;

    public function _get(GetRequest $request, string $id)
    {
        $id = $id == 0 ? $request->getId() : $id;
        $relations = $request->getRelations();
        $model = $this->service->getModel($id, $relations, $request->all());

        $responseFactory = $this->createGetResponseFactory();
        if ($model != null) {
            $response = $responseFactory->getResponse(1, 0, ['data' => $model]);
        } else {
            $response = $responseFactory->getResponse(0, 1);
        }

        return $response->response();
    }

    public function _post(PostRequest $request)
    {
        $new_models = [];
        $failed = [];
        foreach ($request->all() as $index => $value) {
            $new_model = $this->service->createNewModel($value);
            if ($new_model != null) {
                array_push($new_models, $new_model);
            } else {
                array_push($failed, $index);
            }
        }

        $responseFactory = $this->createPostResponseFactory();
        $response = $responseFactory->getResponse(count($new_models), count($failed),
            ['list_data' => [
                'success' => $new_models,
                'failed' => $failed
            ]]);
        return $response->response();
    }

    public function _patch(PatchRequest $request)
    {
        $success = [];
        $failed = [];

        foreach ($request->all() as $value) {
            $id = (int)$value['id'];
            unset($value['id']);
            $updated = $this->service->updateModel($id, $value);
            if ($updated) {
                array_push($success, $id);
            } else {
                array_push($failed, $id);
            }
        }

        $responseFactory = $this->createPatchResponseFactory();
        $response = $responseFactory->getResponse(count($success), count($failed),
            ['list_data' => [
                'success' => $success,
                'failed' => $failed
            ]]);
        return $response->response();
    }

    public function _delete(DeleteRequest $request)
    {
        $success = [];
        $failed = [];

        foreach ($request->all() as $value) {
            $id = (int)$value['id'];
            unset($value['id']);
            $deleted = $this->service->deleteModel($id);

            if ($deleted) {
                array_push($success, $id);
            } else {
                array_push($failed, $id);
            }
        }
        $responseFactory = $this->createDeleteResponseFactory();
        $response = $responseFactory->getResponse(count($success), count($failed),
            ['list_data' => [
                'success' => $success,
                'failed' => $failed
            ]]);
        return $response->response();
    }

    protected function _all(GetRequest $request)
    {
        $models = $this->service->getAll($request->getSort(), $request->getOrder(),
            $request->getLimit(), $request->getOffset(), $request->getRelations());
        $total = $this->service->count();
        $response = new BasePaginateResponse($total, $models->toArray());
        return $response->response();
    }

    protected function createGetResponseFactory(): ResponseFactory
    {
        return new SingleGetResponseFactory();
    }

    protected function createPostResponseFactory(): ResponseFactory
    {
        return new ListPostResponseFactory();
    }

    protected function createPatchResponseFactory(): ResponseFactory
    {
        return new ListPatchResponseFactory();
    }

    protected function createDeleteResponseFactory(): ResponseFactory
    {
        return new ListDeleteResponseFactory();
    }

    protected function getServiceMessage(Service $service): string
    {
        /**
         * @var Message $service
         */
        return $service->getMessage();
    }
}
