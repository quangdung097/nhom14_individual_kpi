<?php


namespace App\Http\Controllers\API\Fake;


use App\Http\Controllers\Requests\API\ApproveProgressRequests\ApproveProgressRequest;

class ApproveProgressController extends BaseFakeAPIController
{
    public function post(ApproveProgressRequest $request)
    {
        $new_models = [];
        foreach ($request->all() as $index => $value) {
            array_push($new_models, 'Approve successfully');
        }

        $responseFactory = $this->createPostResponseFactory();
        $response = $responseFactory->getResponse(count($new_models), 0,
            ['list_data' => [
                'success' => $new_models,
                'failed' => []
            ]]);
        $response->setResponseMessage('List approve successfully');
        return $response->response();
    }
}