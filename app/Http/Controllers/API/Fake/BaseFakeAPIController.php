<?php


namespace App\Http\Controllers\API\Fake;


use App\Http\Controllers\Responses\ResponseFactories\GetResponseFactories\SingleGetResponseFactory;
use App\Http\Controllers\Responses\ResponseFactories\ModifiedResponseFactories\PostResponseFactories\ListPostResponseFactory;
use App\Http\Controllers\Responses\ResponseFactories\ResponseFactory;

class BaseFakeAPIController
{
    protected function createGetResponseFactory(): ResponseFactory
    {
        return new SingleGetResponseFactory();
    }

    protected function createPostResponseFactory(): ResponseFactory
    {
        return new ListPostResponseFactory();
    }
}