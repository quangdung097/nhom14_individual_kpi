<?php


namespace App\Http\Controllers\API\Fake;


use App\Http\Controllers\Requests\API\AuthorizationRequests\EncodePasswordRequest;
use App\Http\Controllers\Responses\ImpResponses\BaseDataSuccessResponse;

class EncodedPasswordController extends BaseFakeAPIController
{
    public function post(EncodePasswordRequest $request)
    {
        $request->get('password');

        $response = new BaseDataSuccessResponse();
        $response->setResponseMessage('Password encoded.');
        $response->setResponseData([
            'encode_password' => '5f4dcc3b5aa765d61d8327deb882cf99'
        ]);

        return $response->response();
    }
}