<?php


namespace App\Http\Controllers\API\Fake;


use App\Http\Controllers\Requests\API\AuthorizationRequests\LoginRequest;
use App\Http\Controllers\Requests\API\AuthorizationRequests\RetrieveUserRequest;
use App\Http\Controllers\Responses\ImpResponses\BaseDataSuccessResponse;

class RetrieveUserController extends BaseFakeAPIController
{
    public function get(RetrieveUserRequest $request)
    {
        $response = new BaseDataSuccessResponse();
        $response->setResponseMessage('Found it');
        $response->setResponseData([
            'name' => 'Ivan Ivanovich',
            'email' => 'russland@gmail.com'
        ]);

        return $response->response();
    }

    public function post(LoginRequest $request)
    {
        $response = new BaseDataSuccessResponse();
        $response->setResponseMessage('Login successfully');
        $response->setResponseData([
            'name' => 'Ivan Ivanovich',
            'email' => 'russland@gmail.com',
            'access_token' => 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJodHRwczovL2V4YW1wbGUuYXV0aDAuY29tLyIs
                ImF1ZCI6Imh0dHBzOi8vYXBpLmV4YW1wbGUuY29tL2NhbGFuZGFyL3YxLyIsInN1YiI6InVzcl8xMjMiLCJpYXQiOjE0NTg3ODU3OTYs
                ImV4cCI6MTQ1ODg3MjE5Nn0.CA7eaHjIHz5NxeIJoFK9krqaeZrPLwmMmgI_XiQiIkQ'

        ]);

        return $response->response();
    }
}