<?php


namespace App\Http\Controllers\Requests\API\UserRequests;


use App\Http\Controllers\Requests\API\PostRequest;

class UserPostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'name' => 'string|required|max:255',
            'email' => 'email|required|unique:users,email|distinct',
            'password' => 'string|required|max:255'
        ];
    }
}