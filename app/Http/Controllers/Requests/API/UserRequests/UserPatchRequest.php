<?php


namespace App\Http\Controllers\Requests\API\UserRequests;


use App\Http\Controllers\Requests\API\PatchRequest;

class UserPatchRequest extends PatchRequest
{
    public function rules(): array
    {
        return [
            '*.id' => 'int|required|exists:users,id',
            '*.name' => 'string|max:255',
            '*.email' => 'email|unique:users,email|distinct',
            '*.password' => 'string|max:255'
        ];
    }
}