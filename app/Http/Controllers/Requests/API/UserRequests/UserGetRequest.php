<?php


namespace App\Http\Controllers\Requests\API\UserRequests;


use App\Http\Controllers\Requests\API\GetRequest;
use Illuminate\Http\Request;

class UserGetRequest extends GetRequest
{

    protected function relations(): array
    {
        return ['department'];
    }

    protected function sort(): array
    {
        return ['id'];
    }

    protected function filterRules(): array
    {
        return [
            'from' => 'date',
            'to' => 'date|after:from',
            'project_id' => 'string',
            'task_id' => 'string',
        ];
    }

    public function getFrom(): string
    {
        return Request::get('from') ?: date('Y-m-d', strtotime("-7 days"));
    }

    public function getTo(): string
    {
        return Request::get('to') ?: date('Y-m-d', time());
    }

    public function getTaskId(): ?string
    {
        return Request::get('task_id') ?: null;
    }

    public function getProjectId(): ?string
    {
        return Request::get('project_id') ?: null;
    }
}
