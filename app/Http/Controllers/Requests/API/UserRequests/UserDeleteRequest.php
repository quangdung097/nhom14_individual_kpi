<?php


namespace App\Http\Controllers\Requests\API\UserRequests;


use App\Http\Controllers\Requests\API\DeleteRequest;

class UserDeleteRequest extends DeleteRequest
{
    public function rules(): array
    {
        return [
            '*.id' => 'int|required|exists:users,id',
            '*' => 'distinct',
        ];
    }
}