<?php


namespace App\Http\Controllers\Requests\API\FormulaRequests;


use App\Http\Controllers\Requests\API\PostRequest;

class FormulaPostRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            '*.name' => 'string|unique:formulas,name|required',
            '*.description' => 'string|required',
        ];
    }
}
