<?php


namespace App\Http\Controllers\Requests\API\FormulaRequests;


use App\Http\Controllers\Requests\API\GetRequest;

class FormulaGetRequest extends GetRequest
{

    protected function relations(): array
    {
        return [];
    }

    protected function sort(): array
    {
        return [];
    }

    protected function filterRules(): array
    {
        return [];
    }
}
