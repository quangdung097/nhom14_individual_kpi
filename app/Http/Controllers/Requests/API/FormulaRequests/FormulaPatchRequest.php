<?php


namespace App\Http\Controllers\Requests\API\FormulaRequests;


use App\Http\Controllers\Requests\API\PatchRequest;

class FormulaPatchRequest extends PatchRequest
{
    public function rules(): array
    {
        return [
            '*.name' => 'string|unique:formulas,name',
            '*.description' => 'string',
            '*.id' => 'int|required|exists:formulas,id'
        ];
    }
}
