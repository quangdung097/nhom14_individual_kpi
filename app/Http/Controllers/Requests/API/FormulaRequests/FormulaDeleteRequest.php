<?php


namespace App\Http\Controllers\Requests\API\FormulaRequests;


use App\Http\Controllers\Requests\API\DeleteRequest;

class FormulaDeleteRequest extends DeleteRequest
{

    public function rules(): array
    {
        return [
            '*.id' => 'int|required|exists:formulas,id|distinct'
        ];
    }

}
