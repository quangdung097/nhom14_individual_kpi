<?php


namespace App\Http\Controllers\Requests\API;


use App\Http\Controllers\Requests\RestfulRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

abstract class GetRequest extends RestfulRequest
{
    protected abstract function relations(): array;

    protected abstract function sort(): array;

    protected abstract function filterRules(): array;

    public function rules()
    {
        return array_merge([
            'id' => 'int',
            'q' => 'string',
            'limit' => 'int|min:1|max:100',
            'offset' => 'int|min:0',
            'order' => 'string|in:asc,desc',
            'sort' => ['string', Rule::in($this->sort())],
            'relations' => 'array',
            'relations.*' => ['string', Rule::in($this->relations())]
        ], $this->filterRules());
    }

    public function getId(): ?int
    {
        return Request::get('id');
    }

    public function getQuery(): ?string
    {
        return Request::get('q');
    }

    public function getOffset(): int
    {
        return Request::get('offset') ?: 0;
    }

    public function getLimit(): int
    {
        return Request::get('limit') ?: 20;
    }

    public function getOrder(): string
    {
        return Request::get('order') ?: 'asc';
    }

    public function getSort(): string
    {
        return Request::get('sort') ?: 'id';
    }

    public function getRelations(): array
    {
        $relations = Request::get('relations');
        return $relations ? $relations : [];
    }

    public function getFilterRules(): array
    {
        return [];
    }
}