<?php


namespace App\Handlers;


use App\Services\Service;

abstract class BaseEloquentHandler extends BaseHandler
{
    protected $handlerService;

    public function __construct()
    {
        $this->handlerService = $this->createHandlerService();
    }

    abstract protected function createHandlerService(): Service;

    protected function getService()
    {
        return $this->handlerService;
    }
}