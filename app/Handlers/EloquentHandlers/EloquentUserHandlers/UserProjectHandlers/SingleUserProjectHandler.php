<?php


namespace App\Handlers\EloquentHandlers\EloquentUserHandlers\UserProjectHandlers;


use App\Handlers\EloquentHandlers\ImpHandlers\HttpGetHandler;

class SingleUserProjectHandler extends UserProjectHandler
{
    public function handle(array $attributes): array
    {
        $attributes['url'] =
            'http://3.1.20.54/v1/tasks?project_id='.$attributes['project_id'].'&user_id='.$attributes['user_id'];
        $getHandler = new HttpGetHandler();
        $response = $getHandler->handle($attributes);
        $projectData = $this->getProject($response['data']['results'], $attributes['user_id']);
        $attributes['project'] = $projectData;

        $this->formatAfterAttributes($attributes);
        return parent::handle($attributes);
    }

    private function getProject(array $project, string $user_id)
    {
        $project = $project[0]['detail_project'];
        return $this->formatProject($project, $user_id);
    }
}
