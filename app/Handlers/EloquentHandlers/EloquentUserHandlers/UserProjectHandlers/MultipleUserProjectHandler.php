<?php


namespace App\Handlers\EloquentHandlers\EloquentUserHandlers\UserProjectHandlers;


use App\Handlers\EloquentHandlers\ImpHandlers\HttpGetHandler;

class MultipleUserProjectHandler extends UserProjectHandler
{
    public function handle(array $attributes): array
    {
        $from = strtotime($attributes['from']);
        $to = strtotime($attributes['to']);
        $attributes['url'] =
            'http://3.1.20.54/v1/users/'.$attributes['user_id'].'/projects?created_from='.$from.'&created_to='.$to;
        $getHandler = new HttpGetHandler();
        $response = $getHandler->handle($attributes);

        $projectData = $this->getProject($response['data']['results'], $attributes['user_id']);
        $attributes['projects'] = $projectData;
        $this->formatAfterAttributes($attributes);
        $attributes['from'] = date('Y-m-d', strtotime($attributes['from']));
        $attributes['to'] = date('Y-m-d', strtotime($attributes['to']));
        return parent::handle($attributes);
    }

    private function getProject(array $projects, string $user_id)
    {
        foreach ($projects as &$project) {
            $project = $this->formatProject($project, $user_id);
        }
        return $projects;

    }
}
