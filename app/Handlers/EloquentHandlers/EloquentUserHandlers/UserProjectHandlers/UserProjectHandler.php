<?php


namespace App\Handlers\EloquentHandlers\EloquentUserHandlers\UserProjectHandlers;


use App\Handlers\EloquentHandlers\EloquentUserHandlers\EloquentUserHandler;
use App\Handlers\EloquentHandlers\ImpHandlers\HttpGetHandler;

class UserProjectHandler extends EloquentUserHandler
{
    protected function formatProject($project, $user_id): array
    {
        $tmp['id'] = $project['id'];
        $tmp['name'] = $project['name'];
        $tmp['description'] = $project['description'];
        $tmp['tasks'] = $this->getTask($project['id'], $user_id);
        $tmp['KPI'] = $this->calculateKPI($project, $tmp['tasks']);

        $tmp['created_at'] = date('Y-m-d H:i:s', $project['created_time']);
        $tmp['updated_at'] = date('Y-m-d H:i:s', $project['updated_time']);
        $tmp['finished_at'] = date('Y-m-d H:i:s', $project['completed_time']);
        $tmp['required_at'] = date('Y-m-d H:i:s', $project['deadline']);


        return $tmp;
    }

    protected function getTask($project_id, $user_id): array
    {
        $handler = new HttpGetHandler();
        $response =
            $handler->handle(['url' => 'http://3.1.20.54/v1/tasks?project_id=' . $project_id . '&user_id=' . $user_id]);

        $tasks = $response['data'];
        $returnedTasks = [];
        foreach ($tasks['results'] as $task) {
            $tmp['name'] = $task['name'];
            $tmp['description'] = $task['description'];
            $tmp['complete_percent'] = $task['percent_complete'];
            array_push($returnedTasks, $tmp);
        }
        return $returnedTasks;
    }

    protected function calculateKPI($project, $tasks)
    {
        if (strcasecmp($project['status'], 'completed') == 0) {
            if ($project['completed_time'] > $project['deadline']) {
                return 0;
            } else {
                $tasksNumber = count($tasks);
                if ($tasksNumber > 0) {
                    $sum = 0;
                    foreach ($tasks as $task) {
                        $sum += $task['complete_percent'];
                    }
                    return round($sum / $tasksNumber, 3);
                }
                return 0;
            }
        }
        return round((($project['deadline'] - $project['completed_time']) / 10000000), 3);
    }

    protected function formatAfterAttributes(&$attributes)
    {
        unset($attributes['results']);
        unset($attributes['project_id']);
        unset($attributes['url']);
    }

}
