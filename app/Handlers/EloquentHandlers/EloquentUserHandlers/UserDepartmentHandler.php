<?php


namespace App\Handlers\EloquentHandlers\EloquentUserHandlers;


class UserDepartmentHandler extends EloquentUserHandler
{
    private $department = [
        'name' => 'Phòng kho',
    ];

    public function handle(array $attribute): array
    {
        if (in_array('department', $attribute['relations'])) {
            unset($attribute['relations']);
            $attribute['department'] = $this->department;
        }
        return parent::handle($attribute);
    }
}
