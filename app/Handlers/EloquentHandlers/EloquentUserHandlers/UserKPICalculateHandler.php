<?php


namespace App\Handlers\EloquentHandlers\EloquentUserHandlers;


class UserKPICalculateHandler extends EloquentUserHandler
{
    public function handle(array $attribute): array
    {
        $attribute['tasks'] = [
            [
                'name' => "Checking drugs' quality every day 2-2020",
                'description' => "Every day, employees need to make sure the quality of all drug products are good.",
                'complete_percent' => 50,
                'KPI' => $this->getKPI(),
                'important' => 0.6
            ],
            [
                'name' => "Collect drug's quantity reports 2-2020",
                'description' => "Collect reports from local suppliers",
                'complete_percent' => 90,
                'KPI' => $this->getKPI(),
                'important' => 0.4
            ],
            [
                'name' => "Checking drugs' quality every day 3-2020",
                'description' => "Every day, employees need to make sure the quality of all drug products are good.",
                'complete_percent' => 70,
                'KPI' => $this->getKPI(),
                'important' => 0.6
            ],
            [
                'name' => "Collect drug's quantity reports 3-2020",
                'description' => "Collect reports from local suppliers",
                'complete_percent' => 80,
                'KPI' => $this->getKPI(),
                'important' => 0.3
            ],
            [
                'name' => "Bribe local authorities 3-2020",
                'description' => "A little help to smooth things up",
                'complete_percent' => 65,
                'KPI' => $this->getKPI(),
                'important' => 0.1
            ],
        ];
        $attribute['KPI'] = $this->getKPI();
        return parent::handle($attribute);
    }

    private function getKPI(): float
    {
        return rand(0, 100) / 100;
    }
}
