<?php


namespace App\Handlers\EloquentHandlers\EloquentUserHandlers;


use App\Handlers\BaseEloquentHandler;
use App\Services\EloquentServices\EloquentUserService;
use App\Services\Service;

class EloquentUserHandler extends BaseEloquentHandler
{
    protected function createHandlerService(): Service
    {
        return new EloquentUserService();
    }
}
