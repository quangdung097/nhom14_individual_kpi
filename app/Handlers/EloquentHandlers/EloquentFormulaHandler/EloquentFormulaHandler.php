<?php


namespace App\Handlers\EloquentHandlers\EloquentFormulaHandler;


use App\Handlers\BaseEloquentHandler;
use App\Services\EloquentServices\EloquentFormulaService;
use App\Services\Service;

class EloquentFormulaHandler extends BaseEloquentHandler
{

    protected function createHandlerService(): Service
    {
        return new EloquentFormulaService();
    }
}
