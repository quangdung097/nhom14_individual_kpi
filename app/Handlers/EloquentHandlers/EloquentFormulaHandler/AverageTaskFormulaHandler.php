<?php


namespace App\Handlers\EloquentHandlers\EloquentFormulaHandler;


class AverageTaskFormulaHandler extends EloquentFormulaHandler
{
    public function handle(array $attribute): array
    {
        $tasks = $attribute['tasks'];

        $sum = 0;
        foreach ($tasks as $task) {
            $sum += $task['completed_percent'];
        }

        if (count($tasks) == 0) {
            $attribute['KPI'] = 0;
        } else {
            $attribute['KPI'] = $sum / count($tasks);
        }

        return parent::handle($attribute);
    }
}
