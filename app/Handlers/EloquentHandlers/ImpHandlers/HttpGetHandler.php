<?php


namespace App\Handlers\EloquentHandlers\ImpHandlers;


use App\Handlers\BaseHandler;

class HttpGetHandler extends BaseHandler
{
    public function handle(array $attribute): array
    {
        if (array_key_exists('url', $attribute)) {
            $url = $attribute['url'];
            $attribute['results'] = $this->httpGet($url);
            return parent::handle($attribute);
        }
        return $this->createHandlerResponse(false);
    }

    private function httpGet(string $url): array
    {
        $jsonResult = json_decode(file_get_contents($url), true);
        if ($jsonResult['count'] > 0) {
            return $jsonResult['results'];
        }
        return [];
    }
}
