<?php


namespace App\Handlers;


interface Handler
{
    public function handle(array $attribute): array;

    public function setNextHandler(Handler $nextHandler);
}