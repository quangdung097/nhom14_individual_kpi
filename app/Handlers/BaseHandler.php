<?php


namespace App\Handlers;


class BaseHandler implements Handler
{
    /**
     * @var Handler $nextHandler
     */
    private $nextHandler;

    public function handle(array $attribute): array
    {
        if ($this->nextHandler != null) {
            return $this->nextHandler->handle($attribute);
        }
        return $this->createHandlerResponse(true, 'Done', $attribute);
    }

    public function setNextHandler(Handler $nextHandler)
    {
        $this->nextHandler = $nextHandler;
    }

    protected function createHandlerResponse(bool $status = true,
                                             string $message = 'Done',
                                             array $attribute = []): array
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $attribute
        ];
    }
}