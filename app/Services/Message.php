<?php


namespace App\Services;


interface Message
{
    public function getMessage(): string;
}