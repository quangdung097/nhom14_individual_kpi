<?php


namespace App\Services;


use Illuminate\Database\Eloquent\Model;

interface Service extends Message
{
    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model;

    public function createNewModel(array $attributes): ?Model;

    public function updateModel(int $id, array $attributes): bool;

    public function deleteModel(int $id): bool;

    public function count(array $attributes = []): int;

    public function getAll(string $sort, string $order, int $limit, int $offset, array $relations);

    public function findModels(array $pairs = [], array $relations = []);
}
