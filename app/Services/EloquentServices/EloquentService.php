<?php


namespace App\Services\EloquentServices;


use App\Services\Service;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

abstract class EloquentService implements Service
{
    private $queryBuilder;
    private $message;

    protected $manyToManyRelations = [];

    public function __construct()
    {
        $this->queryBuilder = $this->createQueryBuilder();
    }

    public function getModel(string $id, array $relations = [], array $attributes = []): ?Model
    {
        if (isset($relations)) {
            return $this->getQuery()->with($relations)->find($id);
        }
        return $this->getQuery()->find($id);
    }

    public function createNewModel(array $attributes): ?Model
    {
        $model = null;
        try {
            DB::transaction(function () use ($attributes, &$model) {
                $model = $this->queryBuilder->create($attributes);
                foreach ($this->manyToManyRelations as $relation) {
                    if (method_exists($model, $relation) && array_key_exists($relation, $attributes)) {
                        $model->$relation()->sync($attributes[$relation]);
                    }
                }
            });
        } catch (\Exception $exception) {
            $this->setMessage($exception->getMessage());
            return null;
        }
        return $model;
    }

    public function updateModel(int $id, array $attributes): bool
    {
        try {
            DB::transaction(function () use ($id, $attributes) {
                $model = $this->getQuery()->findOrFail($id);
                $model->update($attributes);
                foreach ($this->manyToManyRelations as $relation) {
                    if (method_exists($model, $relation) && array_key_exists($relation, $attributes)) {
                        $model->$relation()->sync($attributes[$relation]);
                    }
                }
            });
            return true;
        } catch (\Exception $exception) {
            $this->setMessage($exception->getMessage());
            return false;
        }
    }

    public function deleteModel(int $id): bool
    {
        return $this->queryBuilder->getModel()::destroy($id);
    }

    public function count(array $attributes = []): int
    {
        return $this->getQuery()->count();
    }

    public function getAll(string $sort = 'id',
                           string $order = 'asc', int $limit = 5,
                           int $offset = 0, array $relations = [])
    {
        return $this->getQuery()->orderBy($sort, $order)
            ->limit($limit)->offset($offset)
            ->with($relations)->get();
    }

    public function findModels(array $pairs = [], array $relations = [])
    {
        $query = $this->getQuery();
        try {
            foreach ($pairs as $pair) {
                $query = $query->where([$pair[0], $pair[1], $pair[2]]);
            }
        } catch (\Exception $exception) {
            $this->setMessage($exception->getMessage());
            return new Collection();
        }
        if (count($relations) > 0) {
            return $query->with($relations)->get();
        }
        return $query->get();
    }

    protected function getQuery(): Builder
    {
        return clone $this->queryBuilder;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    protected function setMessage(string $message)
    {
        $this->message = $message;
    }

    abstract protected function createQueryBuilder(): Builder;
}
