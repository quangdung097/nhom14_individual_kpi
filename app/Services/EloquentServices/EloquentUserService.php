<?php


namespace App\Services\EloquentServices;


use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class EloquentUserService extends EloquentService
{

    protected function createQueryBuilder(): Builder
    {
        return User::query();
    }
}
