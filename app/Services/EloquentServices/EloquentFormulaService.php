<?php


namespace App\Services\EloquentServices;


use App\Models\Formula;
use Illuminate\Database\Eloquent\Builder;

class EloquentFormulaService extends EloquentService
{

    protected function createQueryBuilder(): Builder
    {
        return Formula::query();
    }
}
